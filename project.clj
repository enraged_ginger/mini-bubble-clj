(defproject mini-bubble-clj "0.1.0-SNAPSHOT"
  :description "Does some bubble sort magic in the fewest bytes possible."
  :url "http://www.bitbucket.org/enraged_ginger/mini-bubble-clj"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]]
  :main h/m)
