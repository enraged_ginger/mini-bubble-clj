# mini-bubble-clj

A Clojure application for doing a bubble sort in the fewest number of bytes possible.

## Usage

Pass any strings that you want ordered to the run command like so:
`lein run 1 2 face str ats sta`

## License

Copyright © 2015 Stephen M. Hopper

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
